"use strict";

var graphNode = function(id) {

    return {
        id: id,
        visited: false
    };
};

var graph = function() {
    /* A graph implemented in that adjacency list style */

    var that, nodes, edges;

    that = {};

    // Object containing nodes in the graph referenced by their id
    nodes = [];

    // Object containing the edge list for each node referenced by id
    edges = {};

    // Add the supplied graphNode to the graph
    that.addNode = function(n) {
        nodes.push(n);
    };

    // Retrieve the node by its id
    that.getNode = function(id) {
        return nodes[id];
    };

    // Add an edge list to the graph referenced by its associated node
    that.addEdges = function(n, e) {
        edges[n.id] = e;
    };

    // Retrieve the edge list associated with a particular node
    that.getEdges = function(n) {
        return edges[n.id];
    };

    // Set the visited flag of each node in the graph to false
    that.unsetVisited = function() {
        nodes.map(function(n) {n.visited = false;});
    };

    /* Implement that.dfs */

    that.dfs = function(n) {

    };

    /* Implement that.dfs */

    return that;
};


var g = graph();

var n0, n1, n2, n3, n4, n5, n6;

n0 = graphNode(0);
n1 = graphNode(1);
n2 = graphNode(2);
n3 = graphNode(3);
n4 = graphNode(4);
n5 = graphNode(5);
n6 = graphNode(6);

g.addNode(n0);
g.addNode(n1);
g.addNode(n2);
g.addNode(n3);
g.addNode(n4);
g.addNode(n5);
g.addNode(n6);

g.addEdges(n0, [n2, n5]);
g.addEdges(n1, [n1, n5]);
g.addEdges(n2, [n0, n3]);
g.addEdges(n3, [n0, n1]);
g.addEdges(n4, [n6]);
g.addEdges(n5, [n1]);
g.addEdges(n6, [n4]);


console.log(g.dfs(g.getNode(0)));

console.log(g.dfs(g.getNode(4)));
