/**
 * Created by jesus on 3/1/16.
 */
"use strict";
function LiveLinks(fbname) {
    var firebase = new Firebase("https://"+ fbname + ".firebaseio.com");
    this.firebase = firebase;
    this.linksRef = this.firebase.child('livelinks');
    this.submitLink = function(url,name) {
        if ((url.substring(0,5) !== "https") && (url.substring(0,4) !== "http")) {
            url = "http://" + url;
        }
        this.linksRef.child(btoa(url)).set({
            name: name
        });
    };
}

$(function() {
    var ll = new LiveLinks("blistering-fire-6598");
    $('#link-form').on('submit',function(e) {
        e.preventDefault();
        ll.submitLink($(this).find('#link-url').val(), $(this).find('#link-name').val());
        $(this).find("input[type=text]").val("").blur();
        e.stopPropagation();
    });

    ll.linksRef.on('child_added',function(snapshot) {
        var link = snapshot.val();
        var url = snapshot.key();
        console.log("child_added",url,atob(url),link);
        url = atob(url);
        $('#list').append($('<li class="hot">'+link.name+ ' : ' + url + '</li>' ));
    });
});