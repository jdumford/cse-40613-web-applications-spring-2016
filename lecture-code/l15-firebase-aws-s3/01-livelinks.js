/**
 * Created by jesus on 3/1/16.
 */
"use strict";

function LiveLinks(fbname) {
    this.firebase = new Firebase("https://"+ fbname + ".firebaseio.com");
    this.linksRef = this.firebase.child('livelinks');

    // Data Read Event Listeners
    this.linksRef.on('child_added',function(snapshot) {
        console.log("child_added:",snapshot.val());
    });
    this.linksRef.on('child_changed',function(snapshot) {
       console.log("child_changed:",snapshot.val());
    });
    this.linksRef.on('child_removed',function(snapshot){
        console.log("child_removed:",snapshot.val());
    });
    this.linksRef.on('value',function(snapshot) {
        console.log("value called last:",snapshot.val());
    });

    // Data Write Methods
    this.newLinkRef = this.linksRef.push({url:"www.nd.edu",name:"University of Notre Dame"});
    console.log("key for link:",this.newLinkRef.key());
    this.newLinkRef.update({url:"https://www.nd.edu"});
    this.newLinkRef.remove();
    this.newLinkRef.set({url:"https://www.nd.edu"});
}