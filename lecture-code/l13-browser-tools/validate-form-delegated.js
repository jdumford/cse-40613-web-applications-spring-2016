"use strict";

var phonePattern = /\d{3}-\d{3}-\d{4}/;
var emailPattern = /\w+@\w+\.\w+/;

$(function() {
    $("#contact").on("blur", "input, textarea", function(event) {
        var $target, value;

        console.log(event);

        $target = $(event.target);

        console.log($target);

        value = $target.val();

        console.log(value);

        if ($target.attr("id") === "phoneid") {
            if (value === "" || !phonePattern.test(value)) {
                if ($target.hasClass("valid")) {
                    $target.removeClass("valid");
                }
                $target.addClass("invalid");
            }
            else {
                if ($target.hasClass("invalid")) {
                    $target.removeClass("invalid");
                }
                $target.addClass("valid");
            }
        }
        else if ($target.attr("id") === "emailid") {
            if (!emailPattern.test(value)) {
                if ($target.hasClass("valid")) {
                    $target.removeClass("valid");
                }
                $target.addClass("invalid");
            }
            else {
                if ($target.hasClass("invalid")) {
                    $target.removeClass("invalid");
                }
                $target.addClass("valid");
            }
        }
    });
});