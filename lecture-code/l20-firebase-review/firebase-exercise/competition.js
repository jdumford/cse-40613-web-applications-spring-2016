/**
 * Created by jesus on 3/28/16.
 */
"use strict";
function FavoriteCars(fbname) {
    var ref = new Firebase("https://" + fbname + ".firebaseio.com/");
    this.ref = ref;
    var carsRef = ref.child('cars');
    this.carsRef = carsRef;
    var namesRef = ref.child('names');
    this.namesRef = namesRef;
    var instance = this;

    this.vote = function (carId, voteVal) {
        carsRef.child(carId).transaction(function (currentValue) {
            return (currentValue || 0) + +voteVal;
        });
    };
}

$(function () {
    var cars = new FavoriteCars("your-app-here"),
        $vote,
        names;
    // this updates the number of votes in static list
    cars.carsRef.orderByValue().on('value', function (snapshot) {
        var $jQsel;
        snapshot.forEach(function (childSnapshot) {
            var carName = childSnapshot.key(),
                carVotes = childSnapshot.val();
            console.log(carName, carVotes);
            $jQsel = $('#' + carName);
            $jQsel.find("span.badge").html(carVotes);
        });
    });
    // click handler for both up and down votes
    $vote = $('.vote');
    $vote.on('click', function (e) {
        console.log(e.target);
        var $target = $(e.target);
        var $li = $target.parent();
        var carId = $li.attr('id');
        var voteVal = $target.attr('data-val');
        cars.vote(carId, +voteVal);
    });
});