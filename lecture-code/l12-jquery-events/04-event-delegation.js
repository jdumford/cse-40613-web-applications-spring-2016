/**
 * Created by jesus on 2/18/16.
 */
"use strict";
// when user clicks or mouseover items in the list
// except for the fourth item
// it will write out content of the element
// and event information
$(function() {
    var listItem, itemStatus, eventType, timeStamp, $notes = $('#notes');
    var notesMsg = "Click or mouseover restaurant", $lis=$('ul');
    $notes.css({'display':'inline-block'});
    $notes.text(notesMsg);
    $('ul').on(
      'click mouseover',
        ':not(#four)',
        {status: 'important'},
        function(e) {
            listItem = 'Item: ' + e.target.textContent + '<br />';
            itemStatus = 'Status: ' + e.data.status + '<br />';
            eventType = 'Event: ' + e.type + '<br />';
            timeStamp = 'Timestamp: ' + timeConverter(e.timeStamp);
            $notes.html(listItem + itemStatus + eventType + timeStamp);
        }
    );
    $('ul').on('mouseout', function() {
        $notes.html(notesMsg);
    });
});

function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}