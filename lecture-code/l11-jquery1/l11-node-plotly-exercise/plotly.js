"use strict";
/* replace yourUsername, yourKey and yourToken with those items
   after you sign up in plotly and obtain a stream token */

var APP = (function() {
    // Set your username and API key here
    var plotly = require('plotly')('yourUsername', 'yourKey');
    // Set your Streaming API Token here
    var data = [{x:[], y:[], line: {dash: "solid", width: 7}, stream:{token:'yourToken', maxpoints:366}}];

    var graphOptions = {fileopt : "overwrite", filename : "example"};

    plotly.plot(data,graphOptions,function() {
        // Set your Streaming API Token here
        var stream = plotly.stream('yourToken', function (err,res) {
            console.log(err,res);
            clearInterval(loop);
        });
        var i=0;
        var loop = setInterval(function () {
            var arg = i/180*Math.PI;
            var s = Math.sin(arg);
            var streamObject = JSON.stringify({ x : arg, y : s });
            stream.write(streamObject+'\n');
            i++;
        }, 100);
    });
}());
