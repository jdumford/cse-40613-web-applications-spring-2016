/**
 * Created by jesus on 3/31/16.
 * This example illustrates sorting list items in place using jQuery
 */
"use strict";
$(function () {
    var cars = new Firebase("https://myvideo-app.firebaseio.com"),
        $vote,
        names,
        carsRef = cars.child('cars');
    // this updates the number of votes in static list
    carsRef.orderByValue().once('value', function (snapshot) {
        var $jQsel;
        snapshot.forEach(function (childSnapshot) {
            var carName = childSnapshot.key(),
                carVotes = childSnapshot.val();
            console.log(carName, carVotes);
            $jQsel = $('#' + carName);
            $jQsel.find("span.badge").html(carVotes);
        });
        // sort list items according to number of votes
        var $li = $("li");
        $li.detach().sort(function (a, b) {
            var va = +$(a).find('span.badge').html(),
                vb = +$(b).find('span.badge').html();
            // va  > vb sorts in ascending order
            // va < vb sorts in descending order
            //return va > vb;
            return va < vb;
        });
        console.log("sorted:",$li);
        $('ul').append($li);
    });
});