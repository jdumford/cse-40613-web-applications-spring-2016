/**
 * Created by jesus on 2/3/16.
 */
"use strict";

function Hobbit(name, goto, mealsPerDay) {
    this.name = name;
    this['goto'] = goto;
    this.mealsPerDay = mealsPerDay;
    this.fortune = function () {
        var result = "";
        switch(this.goto) {
            case "The Shire":
                result = "Lucky you!";
                break;
            case "Mordor":
                result = "May the Force be with you!";
                break;
            default:
                result = "Unknown destination!";
        }
        return result;
    };
}

var sam = new Hobbit("Samwise Gamgee", "The Shire", 5);
var frodo = new Hobbit("Frodo Baggins", "Mordor", 4);
sam.boss = frodo;
console.log(sam);

var obj={
    id:5,
    contact:{name:'Joe Smith', email:'joe@smith.com'}
};

var ref = obj;
ref.id=6;
console.log(obj.id);
var copycat = {
    id:5,
    contact:{name:'Joe Smith', email:'joe@smith.com'}
};
console.log(ref===obj);
console.log(obj===copycat);

var obj2={};
var obj3=new Object();