/**
 * Created by jesus on 2/1/16.
 */
"use strict";

// Create list of lower case words from a multi-line string
// Eliminate non-word characters and end of lines
// Count the words

//noinspection JSLint
var poem= "Whose woods these are I think I know.\n "+
"His house is in the village, though;\n"+
"He will not see me stopping here\n"+
"To watch his woods fill up with snow.\n"+
    "My little horse must think it queer\n"+
"To stop without a farmhouse near\n"+
"Between the woods and frozen lake\n"+
"The darkest evening of the year.\n\n"+
    `He gives his harness bells a shake
To ask if there is some mistake.
The only other sound's the sweep
Of easy wind and downy flake.
The woods are lovely, dark and deep,
But I have promises to keep,
And miles to go before I sleep,
And miles to go before I sleep.`,
    words, i, counts={};

console.log(poem);

poem = poem.toLowerCase().replace(/\W+/gm,' ').replace(/(\r\n|\n|\r)/gm," ").trim();
words=poem.split(" ");
console.log(words);

// Count the words

for (i=0;i<words.length;i++) {
    counts[words[i]] = counts[words[i]] || 0;
    counts[words[i]]++;
}

console.log(counts);
var n=+"42";
console.log(typeof(n));
