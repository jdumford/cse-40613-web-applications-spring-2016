/**
 * Created by jesus on 3/15/16.
 */
"use strict";
// illustrate use of queries and limits in Firebase

$(function () {

    var ref = new Firebase("https://blistering-fire-6598.firebaseio.com").child('livelinks');

    var $loginButton = $('#login-button'),
        $signupButton = $('#signup-button'),
        $logoutButton = $('#logout-button'),
        $loginForm = $('#login-form'),
        $signupForm = $('#signup-form'),
        $alerts;

    $loginButton.hide();
    $signupButton.hide();
    $logoutButton.hide();
    $loginForm.hide();
    $signupForm.hide();

    // Ordering by a specified child key

    ref.orderByChild("name").on("child_added", function(snapshot){
        var url = atob(snapshot.key()), val = snapshot.val();
        console.log("Title of URL:",url,"is",val.name);
        $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    });
    //ref.orderByChild("createdAt").on("child_added", function(snapshot){
    //    var url = atob(snapshot.key()), val = snapshot.val();
    //    console.log("URL:",url,"created on",val.createdAt);
    //    $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    //});

    // Ordering by a specified child key name
    //ref.orderByKey().on("child_added", function(snapshot){
    //    var url = atob(snapshot.key()), val = snapshot.val();
    //    console.log("URL:",url,"key is ",snapshot.key());
    //    $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    //});

    // Limit queries
    //ref.orderByChild("name").limitToFirst(2).on("child_added", function(snapshot){
    //    var url = atob(snapshot.key()), val = snapshot.val();
    //    console.log("Title of URL:",url,"is",val.name);
    //    $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    //});
    //ref.orderByChild("name").limitToLast(2).on("child_added", function(snapshot){
    //    var url = atob(snapshot.key()), val = snapshot.val();
    //    console.log("Title of URL:",url,"is",val.name);
    //    $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    //});
    // Range Queries
    //ref.orderByChild("name").equalTo("ESPN").on("child_added", function(snapshot){
    //    var url = atob(snapshot.key()), val = snapshot.val();
    //    console.log("ESPN exists and its URL is:",url);
    //    $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    //});
    //ref.orderByChild("name").startAt("A").endAt("A\uf8ff").on("child_added", function(snapshot){
    //    var url = atob(snapshot.key()), val = snapshot.val();
    //    console.log("This name starts with A:",val.name,url);
    //    $('#list').append($('<li class="hot">' + val.name + ' : <a href="' + url + '">' + url + '</a></li>'));
    //});

});