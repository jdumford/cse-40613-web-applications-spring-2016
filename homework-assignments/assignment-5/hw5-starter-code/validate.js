"use strict";

var test_passwords = [
    "Hxou7p&&3",
    "password",
    ";larJ7",
    "DROWSSAP",
    "K33pI7S@f3",
    ":Belieber1"
];

var i;
for (i=0; i < test_passwords.length; i++) {
    test_passwords[i].validate(8,20,regexArray,success,error);
}

